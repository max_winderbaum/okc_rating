<?php
/**
 * check_name.php
 *
 * PHP version 5
 *
 * @category  Scripts
 * @package   OKC_Rating
 * @author    Max Winderbaum <maxwinderbaum@gmail.com>
 * @copyright None
 * @license   Open
 */

include 'config.php';

$localRun = true;

// Get username either from first argument or POST
if (isset($_POST['username'])) {
    $username = $_POST['username'];
    $localRun = false;
} else {
    $username = $argv[1];
}

if ($localRun) {
    echo $username.PHP_EOL;
} else {
    echo $username."<br>";
}
flush();

$highRating = 10000;
$lowRating = 0;
$currentRating = 5000;
$done = false;

while (!$done) {

    $opts = array(
      'http'=>array(
        'method'=>"GET",
        'header'=>"Accept-language: en\r\n" .
                  "Cookie: authlink=$auth; session=$session\r\n"
      )
    );

    $context = stream_context_create($opts);

    // Open the file using the HTTP headers set above
    $file = file_get_contents(
        "http://www.okcupid.com/match?filter1=25,$currentRating,10000&keywords=$username",
        false,
        $context
    );

    if (preg_match("/View $username&rsquo;s profile/", $file) == 1) {
        //Good. Go up
        $lowRating = $currentRating;
        if ($localRun) {
            echo "LowRating = $lowRating".PHP_EOL;
        } else {
            echo "LowRating = $lowRating<br>";
        }
        flush();
    } else {
        //Bad. Go down
        $highRating = $currentRating;
        if ($localRun) {
            echo "HighRating = $highRating".PHP_EOL;
        } else {
            echo "HighRating = $highRating<br>";
        }
        flush(); 
    }

    $currentRating = ((int)(($highRating - $lowRating) / 2)) + $lowRating;
    if ($localRun) {
        echo "Trying $currentRating".PHP_EOL;
    } else {
        echo "Trying $currentRating<br>";
    }
    flush(); 

    if ($highRating - $lowRating == 1) {
        $done = true;
    }

    $result = array();
}

if ($localRun) {
    echo "The rating is $highRating.".PHP_EOL;
} else {
    echo "<br><br>The rating is $highRating.<br>";
}
flush();
if (!$localRun):
?>

<form name="lookup" action="check_name.php" method="post">
    Username to query: <input type="text" name="username"><br>
    <input type="submit" value="Go!">
</form>

<?php
endif;
