#!/bin/bash

if [ $# -ne 2 ]; then
	echo "You need to supply two arguments: username and password."
	exit 1
fi

wget -q --save-cookies cookies.txt --post-data "username=$1&password=$2&dest=/?" http://www.okcupid.com/login

if [ $? -ne 0 ]; then
	echo "wget failed."
	exit 2
fi

# Grep authlink and session from cookies.txt and write them
# to config.php. Note: requires GNU grep.
authlink=$(grep -oP '(?<=authlink\s)\S+' cookies.txt)
session=$(grep -oP '(?<=session\s)\S+' cookies.txt)

if [ -z "$authlink" ] || [ -z "$session" ]; then
	echo "authlink or session is blank."
	exit 3
fi

cat >./config.php << EOF
<?php
	\$auth = '${authlink}';
	\$session = '$session';
EOF
